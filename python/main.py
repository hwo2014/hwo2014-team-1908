import json
import socket
import sys


class DuaiBot:
    def __init__(self, socket, name, key):
        self.socket = socket
        self.name = name
        self.key = key
        self.car = {}
        self.pieces = []
        self.last_piece = None
        self.first_pieces = True
        self.turbo_info = {'available': False, 'cooldown': None}

    def msg(self, msg_type, data, tick=None):
        if tick is None:
            self.send(json.dumps({'msgType': msg_type, 'data': data}))
        else:
            self.send(json.dumps({'msgType': msg_type, 'data': data, 'gameTick': tick}))

    def send(self, msg):
        self.socket.sendall((msg + "\n").encode('utf-8'))

    def join(self):
        return self.msg('join', {'name': self.name, 'key': self.key})

    def join_race(self, track_name, car_count):
        return self.msg('joinRace', {
            'botId': {
                'name': self.name,
                'key': self.key
            },
            'trackName': track_name,
            'carCount': car_count
        })

    def throttle(self, throttle, tick):
        self.msg('throttle', throttle, tick)

    def switch_lane(self, direction, tick):
        self.msg('switchLane', direction, tick)

    def turbo(self, tick):
        self.msg('turbo', 'Pew', tick)

    def ping(self):
        self.msg('ping', {})

    def run(self):
        self.join_race('keimola', 1)
        self.msg_loop()

    def get_piece(self, i):
        while i >= len(self.pieces):
            i -= len(self.pieces)
        return self.pieces[i]

    def on_your_car(self, data):
        self.car = data
        print('We are {0} ({1})'.format(self.car['name'], self.car['color']))

    def on_game_init(self, data):
        track = data['race']['track']
        print('Racing on {0}'.format(track['name']))
        self.pieces = track['pieces']
        self.ping()

    def on_car_positions(self, data, tick=None):
        # Find our car
        current_piece = None
        current_angle = None
        for i, car in enumerate(data):
            if car['id'] == self.car:
                current_piece = car['piecePosition']['pieceIndex']
                current_angle = car['angle']
                break
        if current_piece is None:
            self.ping()
            return

        # TODO determine the best lane to use
        # TODO push other bots in turns

        # Throttle control
        curp = abs(self.get_piece(current_piece).get('angle', 0))
        prevp = [abs(self.get_piece(current_piece - piece_id).get('angle', 0)) for piece_id in range(1, 5)]
        nextp = [abs(self.get_piece(current_piece + piece_id).get('angle', 0)) for piece_id in range(1, 5)]
        throttle = 1.0

        # Going into turn
        if curp < 30 < nextp[1]:
            throttle = 0.0
        if curp < 30 < nextp[0]:
            throttle = 0.5

        # In turn
        if curp > 30:
            throttle = 0.65

        # Coming from turn
        if nextp[0] < 30 < curp:
            throttle = 1.0

        #if self.turbo_info['cooldown'] is not None and tick is not None \
        #        and tick < self.turbo_info['cooldown']:
        #    throttle = 0.4

        if self.first_pieces:
            throttle = max(0.8, throttle)
        if current_piece > 3:
            self.first_pieces = False

        #if self.turbo_info['available'] and prevp[0] == 0 and sum(nextp) == 0:
        #    print('Turbo!')
        #    self.turbo(tick)
        #    self.turbo_info['available'] = False
        #    self.turbo_info['cooldown'] = tick + self.turbo_info['duration']
        #else:
        self.throttle(throttle, tick)

        if self.last_piece != current_piece:
            print('On piece {0}, throttle: {1}'.format(current_piece, throttle))
            self.last_piece = current_piece

    def on_crash(self, data, tick=None):
        print('{0} ({1}) crashed'.format(data['name'], data['color']))
        self.ping()

    def on_lap_finished(self, data, tick=None):
        print('{0} ({1}) finished lap {2} in {3} s'.format(
            data['car']['name'],
            data['car']['color'],
            data['lapTime']['lap'],
            data['lapTime']['millis'] / 1000
        ))
        self.ping()

    def on_turbo_available(self, data, tick=None):
        print('Turbo available')
        self.turbo_info['available'] = True
        self.turbo_info['duration'] = data['turboDurationTicks']
        self.turbo_info['factor'] = data['turboFactor']
        self.ping()

    def on_error(self, data):
        print('Error: {0}'.format(data))
        self.ping()

    def msg_loop(self):
        msg_map = {
            'yourCar': self.on_your_car,
            'gameInit': self.on_game_init,
            'carPositions': self.on_car_positions,
            'crash': self.on_crash,
            'lapFinished': self.on_lap_finished,
            'turboAvailable': self.on_turbo_available,
            'error': self.on_error
        }
        socket_file = s.makefile()
        line = socket_file.readline()
        while line:
            msg = json.loads(line)
            msg_type, data = msg['msgType'], msg['data']
            if msg_type in msg_map:
                if 'gameTick' in msg:
                    msg_map[msg_type](data, msg['gameTick'])
                else:
                    msg_map[msg_type](data)
            else:
                print('Got {0}'.format(msg_type))
                self.ping()
            line = socket_file.readline()


if __name__ == '__main__':
    if len(sys.argv) != 5:
        print('Usage: ./run host port botname botkey')
    else:
        host, port, name, key = sys.argv[1:5]
        print('Connecting with parameters:')
        print('host={0}, port={1}, bot name={2}, key={3}'.format(*sys.argv[1:5]))
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.connect((host, int(port)))
        bot = DuaiBot(s, name, key)
        bot.run()
